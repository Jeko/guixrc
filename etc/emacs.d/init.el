;; Logging
(setq warning-minimum-level :error)

;; Gestion des paquets

(require 'package)
(setq package-archives '(("elpa" . "https://elpa.gnu.org/packages/")
			 ("melpa" . "https://melpa.org/packages/")))

;; Use-package

(eval-when-compile
  (package-initialize)
  (require 'use-package))

;; Initialisation

(add-to-list 'load-path (concat user-emacs-directory "/site-lisp"))

;; Apparence

;; Maximiser la fenêtre par défaut
(set-frame-parameter (selected-frame) 'fullscreen 'maximized)
(add-to-list 'default-frame-alist '(fullscreen . maximized))

;; Pas de bar de menu.
(menu-bar-mode -1)

;; Pas de scroll-bar.
(when window-system (scroll-bar-mode -1))

;; Pas de tool-bar
(tool-bar-mode -1)

;; Pas de tooltips
(tooltip-mode -1)


;; Pas de message d'accueil au démarrage.
(setq inhibit-startup-message t
      initial-scratch-message nil
      ring-bell-function 'ignore)

;; Clochette d'alerte visuelle
(setq visible-bell t)

;; Afficher la colonne du curseur
(column-number-mode 1)

;; Divers

;; Y or n au lieu de yes or no
(defalias 'yes-or-no-p 'y-or-n-p)

;; Avoir les accents circonflexes
(load-library "iso-transl")

;; Comportement

;; Défilement
(setq mouse-wheel-scroll-amount '(1 ((shift) . 1)))
(setq mouse-wheel-progressive-speed nil)
(setq mouse-wheel-follow-mouse 't)
(setq scroll-step 1)
(setq scroll-conservatively 101)

;; Longues lignes

;; Activer le soft wrap des lignes trop longues
(global-visual-line-mode t)

;; Divers
(setq large-file-warning-threshold nil)
(setq vc-follow-symlinks t)
(setq ad-redefinition-action 'accept)

;; Thème

;; Modus
(use-package emacs
  :config
  (require-theme 'modus-themes) ; `require-theme' is ONLY for the built-in Modus themes

  ;; Add all your customizations prior to loading the themes
  (setq modus-themes-italic-constructs t
        modus-themes-bold-constructs nil)

  ;; Maybe define some palette overrides, such as by using our presets
  (setq modus-themes-common-palette-overrides
        'modus-themes-preset-overrides-intense)

  ;; Load the theme of your choice.
  (load-theme 'modus-operandi t)
  :bind ("<f5>" . modus-themes-toggle))

;; Police d'écriture
(set-face-attribute 'default nil :height 160)

;; Assistance

(use-package which-key
  :init (which-key-mode)
  :diminish which-key-mode
  :config
  (setq which-key-idle-delay 1))

;; Mode line

(use-package all-the-icons)

(use-package doom-modeline
  :init (doom-modeline-mode 1))

(use-package minions
  :hook (doom-modeline-mode . minions-mode))

;; Completion framework

;; Vertico
(use-package vertico
  :init
  (vertico-mode))

(use-package savehist
  :ensure t
  :init
  (savehist-mode))

(use-package emacs
  :init
  (defun crm-indicator (args)
    (cons (concat "[CRM] " (car args)) (cdr args)))
  (advice-add #'completing-read-multiple :filter-args #'crm-indicator)

  (setq minibuffer-prompt-properties
        '(read-only t cursor-intangible t face minibuffer-prompt))
  (add-hook 'minibuffer-setup-hook #'cursor-intangible-mode)

  (setq enable-recursive-minibuffers t))

(use-package orderless
  :init
  (setq completion-styles '(orderless)))

(use-package consult
  :bind (("C-c h" . consult-history)
	 ("C-x b" . consult-buffer)                ;; orig. switch-to-buffer
	 ("C-x p b" . consult-project-buffer)      ;; orig. project-switch-to-buffer
	 ;; Other custom bindings
	 ("M-y" . consult-yank-pop)                ;; orig. yank-pop
	 ("M-g e" . consult-compile-error)
	 ("M-g f" . consult-flymake)               ;; Alternative: consult-flycheck
	 ("M-g l" . consult-line)             ;; orig. goto-line
	 ("M-g M-g" . consult-goto-line))

  :hook (completion-list-mode . consult-preview-at-point-mode)
  :init
  (advice-add #'completing-read-multiple :override #'consult-completing-read-multiple)
  (setq xref-show-xrefs-function #'consult-xref
        xref-show-definitions-function #'consult-xref)
  :config
  (consult-customize
   consult-theme
   consult-ripgrep consult-git-grep consult-grep
   consult-bookmark consult-recent-file consult-xref
   consult--source-bookmark consult--source-recent-file
   consult--source-project-recent-file))

(use-package corfu
  :custom
  (corfu-auto t)          ;; Enable auto completion
  :config
  (global-corfu-mode))

(use-package emacs
  :init
  (setq completion-cycle-threshold 3)
  (setq tab-always-indent 'complete))

(use-package yasnippet
  :load-path "~/.emacs.d/plugins/yasnippet"
  :config
  (yas-reload-all)
  (yas-global-mode))

;; Writing

(use-package darkroom
  :config
  (setq darkroom-text-scale-increase 0))

(defun jk/enter-focus-mode ()
  (interactive)
  (darkroom-mode 1)
  (display-line-numbers-mode 0))

(defun jk/leave-focus-mode ()
  (interactive)
  (darkroom-mode 0)
  (display-line-numbers-mode 1))

(defun jk/toggle-focus-mode ()
  (interactive)
  (if (symbol-value darkroom-mode)
      (jk/leave-focus-mode)
    (jk/enter-focus-mode)))

(use-package emacs
  :bind ("<f6>" . jk/toggle-focus-mode))

;; Org-mode
;; Basics
;; D'après https://orgmode.org/worg/org-tutorials/orgtutorial_dto.html
(define-key global-map "\C-cl" 'org-store-link)
(define-key global-map "\C-ca" 'org-agenda)
(setq org-log-done t)

;; Direnv
(use-package direnv
  :config
  (direnv-mode))

;; My humble Guile IDE
(load (expand-file-name "guile-ide.el" user-emacs-directory))

;; Screencast & Livecoding
(use-package keycast
  :config
  (define-minor-mode keycast-mode
    "Show current command and its key binding in the mode line (fix for use with doom-mode-line)."
    :global t
    (if keycast-mode
        (add-hook 'pre-command-hook 'keycast--update t)
      (remove-hook 'pre-command-hook 'keycast--update)))
  (add-to-list 'global-mode-string '("" keycast-mode-line))
  (keycast-mode 1))

(use-package gif-screencast
  :bind ("<f9>" . gif-screencast-start-or-stop))

;; Pomodoro
;; Utiliser F12 pour lancer un timer.
;; Relancer un timer alors qu'un autre est en cours : demande de confirmation.
(setq org-clock-sound (expand-file-name "ding.wav" user-emacs-directory))
(local-set-key [f12] 'org-timer-set-timer )

;; Auto-save and lock files
(setq backup-directory-alist
      `((".*" . ,temporary-file-directory)))
(setq auto-save-file-name-transforms
      `((".*" ,temporary-file-directory t)))
(setq remote-file-name-inhibit-locks nil)
(setq auto-save-default nil)
(setq create-lockfiles nil)

;; YAML
(use-package yaml-mode)
