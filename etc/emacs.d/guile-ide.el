;; (require 'package)
;; (setq package-archives '(("melpa" . "https://melpa.org/packages/")))

;; J'utilise use-package pour installer et configurer les paquets d'Emacs.
;;  (eval-when-compile
;;    (require 'use-package))

;; Ajouter le dossier site-lisp au load-path.
;; (add-to-list 'load-path "~/.emacs.d/site-lisp/")

;; (setq use-package-always-ensure t)

;; Automatically kill running processes on exit (REPL, terminal, …)
(setq confirm-kill-processes nil)

;; Skribilo
;; Minor mode automatique quand on édite un fichier .skr ou .skb
(add-hook 'find-file-hook
	  (lambda ()
	    (when (or (string= (file-name-extension buffer-file-name) "skr")
		      (string= (file-name-extension buffer-file-name) "skb"))
	      (skribe-mode +1))))
(add-hook 'skribe-mode-hook 'turn-on-auto-fill)
(autoload 'skribe-mode "skribe.el" "Skribe mode." t)

;; Highlight the cursor current line
(global-hl-line-mode +1)

;; VCS
(use-package magit
  :custom
  (magit-display-buffer-function #'magit-display-buffer-same-window-except-diff-v1))

;; Guile (Scheme)
;; Must-have
(show-paren-mode 1)

(use-package paren-face
  :custom (paren-face-regexp "#?[](){}[]")
  :config (global-paren-face-mode 1))

(use-package geiser
  :init
  (progn
    (setq geiser-default-implementation 'guile)
    (setq geiser-active-implementations '(guile))
    (setq geiser-implementations-alist '(((regexp "\\.scm$") guile)))))

(use-package geiser-guile
  :requires geiser
  :config
  (add-to-list 'geiser-guile-load-path "~/Workspace/guix")
  (setq geiser-guile-manual-lookup-nodes
	'("guile"
          "guix")))

(use-package paredit
  :hook ((emacs-lisp-mode . enable-paredit-mode)
	 (lisp-interaction-mode . enable-paredit-mode)
	 (lisp-mode . enable-paredit-mode)
	 (scheme-mode . enable-paredit-mode)
	 (geiser-repl-mode . enable-paredit-mode))
  :config
  (add-hook 'eval-expression-minibuffer-setup-hook #'paredit-mode)
  ;; Thanks to Maxim Cournoyer for the fix https://lists.gnu.org/archive/html/help-guix/2023-01/msg00112.html
  (define-key paredit-mode-map (kbd "RET") nil)              ;; fix non returning REPL prompt
  (define-key paredit-mode-map (kbd "C-j") 'paredit-newline) ;; fix non returning REPL prompt
  )

(use-package aggressive-indent
  :config (global-aggressive-indent-mode 1))

(use-package flycheck
  :init
  (global-flycheck-mode 1))

(use-package flycheck-guile)

(use-package web-mode
  :config
  (add-to-list 'auto-mode-alist '("\\.html\\.tpl\\'" . web-mode))
  :custom
  (web-mode-markup-indent-offset 2)
  (web-mode-css-indent-offset 2)
  (web-mode-code-indent-offset 2))

;; Indentation pour les macros de Guile-Spec
(defun my-scheme-mode-hook ()
  "Customize some macro indentation."
  (put 'describe 'scheme-indent-function 1)
  (put 'context 'scheme-indent-function 1)
  (put 'it 'scheme-indent-function 1)
  (put 'should 'scheme-indent-function 1)
  (put 'should= 'scheme-indent-function 1)
  (put 'should-throw 'scheme-indent-function 1))
(add-hook 'scheme-mode-hook 'my-scheme-mode-hook)

;; Activer le mode Scheme pour les fichiers .guile ou .scm
(add-to-list 'auto-mode-alist '("\\.guile\\'" . scheme-mode))
(add-to-list 'auto-mode-alist '("\\.scm\\'" . scheme-mode))

;; Recherche et remplace
(use-package iedit
  :bind (("C-;" . iedit-mode)))

;; Edition
(use-package multiple-cursors
  :bind ("C-c m c" . 'mc/edit-lines))

;; Pair-programming
;; Lockstep pour suivre le conducteur de la session de pair-programming.
(require 'lockstep)
(global-set-key (kbd "C-c s e") 'turn-on-lockstep)
(global-set-key (kbd "C-c s d") 'turn-off-lockstep)

;; Une couleur par curseur.
(eval-and-compile 
  (load (expand-file-name "site-lisp/colorful-points.el" user-emacs-directory)))
(global-set-key (kbd "C-c c") 'colorful-points-mode)

;; TDD and TCR
;; Met à jour automatiquement le buffer et Dired quand un fichier est modifié par un programme externe.
(global-auto-revert-mode 1)
(add-hook 'dired-mode-hook 'auto-revert-mode)

;; Emacs in terminal
;; Add space between line numbers and buffer content.
;; (setq linum-format "%4d \u2502 ")
(if (not (display-graphic-p))
    (setq linum-format
          (lambda (line)
            (propertize (format (let ((w (length (number-to-string (count-lines (point-min) (point-max))))))
                                  (concat "%" (number-to-string w) "d \u2502 ")) line) 'face 'linum))))

(add-hook 'scheme-mode-hook 'guix-devel-mode)

(defun export-current-symbol ()
  "Add the symbol at point to the list of exported symbols"
  (interactive)
  (let ((symbol (thing-at-point 'symbol t)))
    (save-excursion
      (search-backward "#:export" nil t)
      (forward-list)
      (backward-char)
      (if (thing-at-point 'symbol)
	  (newline))
      (insert symbol))))

(use-package emacs
  :bind ("C-c x" . export-current-symbol))
