GUIXRC_PATH=/home/jeko/Workspace/guixrc

export GUILE_LOAD_PATH="$GUIXRC_PATH:$GUILE_LOAD_PATH"

guix home --cores=2 reconfigure $GUIXRC_PATH/home/home-configuration.scm
