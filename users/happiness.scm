(define-module (guixrc home happiness)
  #:use-module (gnu home)
  #:use-module (gnu packages)
  #:use-module (gnu services)
  #:use-module (guixrc home screen))

(define-public happiness-environment
  (home-environment
   (packages happiness-packages)
   (services happiness-services)))

(define happiness-services
  (list
   (service
    home-dbus-service-type)
   
   (service
    home-bash-service-type
    (home-bash-configuration
     (environment-variables
      (list
       (cons "GIT_AUTHOR_NAME"
	     "Jérémy Korwin-Zmijowski")
       (cons "GIT_AUTHOR_EMAIL"
	     "jeremy@korwin-zmijowski.fr")
       (cons "GIT_COMMITTER_NAME"
	     "Jérémy Korwin-Zmijowski")
       (cons "GIT_COMMITTER_EMAIL"
	     "jeremy@korwin-zmijowski.fr")))
     (aliases
      (list
       (cons "grep" "grep --color=auto")
       (cons "ll" "ls -l")
       (cons "ls" "ls -p --color=auto")))
     (bashrc
      (list
       (local-file "../etc/.bashrc" "bashrc")))
     (bash-profile
      (list
       (local-file "../etc/.bash_profile" "bash_profile")))))
   
   (service
    home-redshift-service-type
    (home-redshift-configuration
     (location-provider 'manual)
     (latitude 43.71)
     (longitude -1.05)))
   
   (service
    home-xdg-configuration-files-service-type
    `(("emacs"
       ,(local-file "../etc/emacs.d" #:recursive? #t))))))

(define happiness-packages
  (append gnome-packages
	  screen-packages
	  devel-packages
	  misc-packages
	  emacs-packages))

happiness-environment
