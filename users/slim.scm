(define-module (guixrc users slim)
  #:use-module (guixrc home emacs))

(define-public slim-environment
  (home-environment
   (packages slim-packages)
   (services slim-services)))

(define slim-services
  (list
   emacs-service))

(define slim-packages
  (append
   misc-packages
   emacs-packages
   devel-packages
   business-packages
   game-packages

   (list
    glibc-locales
    nss-certs)))

slim-environment
