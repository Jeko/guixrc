(define-module (guixrc home screen)
  #:use-module (gnu packages xdisorg))

(define screen-packages
  (list redshift))
