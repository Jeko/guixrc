(use-modules
 (gnu home)
 (gnu home services)
 (gnu home services shells)
 (gnu home services desktop)  
 (gnu packages)
 (gnu packages gnome)
 (gnu packages version-control)
 (gnu packages emacs)
 (gnu packages emacs-xyz)
 (gnu packages xdisorg)
 (gnu packages compression)
 (gnu packages tex)
 (gnu packages skribilo)
 (gnu packages shellutils)
 (gnu services)
 (guix gexp))

(define jeko/packages
  (list polari
	evolution
 	
	git
        texlive-scheme-basic

	direnv
	
	emacs
  	emacs-use-package
	emacs-guix
	emacs-which-key
	emacs-all-the-icons
	emacs-doom-modeline
	emacs-minions
	emacs-keycast
	emacs-gif-screencast
	emacs-yaml-mode
	emacs-vertico
	emacs-orderless
	emacs-consult
        emacs-marginalia
	emacs-corfu
	emacs-magit
	emacs-geiser
	emacs-geiser-guile
        emacs-eros
	emacs-geiser-eros
        emacs-paredit
	emacs-paren-face
	emacs-aggressive-indent
	emacs-flycheck
	emacs-flycheck-guile
	emacs-web-mode
	emacs-iedit
	emacs-yasnippet
	emacs-multiple-cursors
	emacs-darkroom
        emacs-envrc
	emacs-flycheck-grammalecte))

(define jeko/services
  (list
   (service home-dbus-service-type)
   
   (service home-bash-service-type
            (home-bash-configuration
	     (environment-variables
	      (list
	       (cons "GIT_AUTHOR_NAME" "Jérémy Korwin-Zmijowski")
	       (cons "GIT_AUTHOR_EMAIL" "jeremy@korwin-zmijowski.fr")
	       (cons "GIT_COMMITTER_NAME" "Jérémy Korwin-Zmijowski")
	       (cons "GIT_COMMITTER_EMAIL" "jeremy@korwin-zmijowski.fr")))
             (aliases
	      (list
	       (cons "grep" "grep --color=auto")
	       (cons "ll" "ls -l")
	       (cons "ls" "ls -p --color=auto")))
             (bashrc
	      (list
	       (local-file "../etc/.bashrc" "bashrc")))
             (bash-profile
	      (list
	       (local-file "../etc/.bash_profile" "bash_profile")))))))

(home-environment
 (packages jeko/packages)
 (services jeko/services))
