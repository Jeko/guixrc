(define-module (guixrc home emacs)
  #:use-module (gnu packages emacs)
  #:use-module (gnu packages emacs-xyz))

(define-public emacs-packages
  (list
   emacs
   emacs-use-package
   emacs-guix
   emacs-modus-themes
   emacs-which-key
   emacs-all-the-icons
   emacs-doom-modeline
   emacs-minions
   emacs-ox-gfm
   emacs-ox-reveal
   emacs-markdown-mode
   emacs-keycast
   emacs-gif-screencast
   emacs-yaml-mode
   emacs-vertico
   emacs-orderless
   emacs-consult
   emacs-corfu
   emacs-magit
   emacs-geiser
   emacs-geiser-guile
   emacs-paredit
   emacs-paren-face
   emacs-aggressive-indent
   emacs-flycheck
   emacs-flycheck-guile
   emacs-web-mode
   emacs-iedit
   emacs-yasnippet
   emacs-yasnippet-snippets
   emacs-multiple-cursors))

(define-public emacs-service
  (service
   home-xdg-configuration-files-service-type
   `(("emacs"
      ,(local-file "../etc/emacs.d" #:recursive? #t)))))
