(define-module (guixrc home misc)
  #:use-module (gnu packages tex))

(define misc-packages
  (list texlive-base))
