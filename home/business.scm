(define-module (guixrc home business)
  #:use-module (gnu packages gnucash))

(define business-packages
  (list gnucash))
