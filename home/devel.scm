(define-module (guixrc home devel)
  #:use-module (gnu packages version-control))

(define devel-packages
  (list git))
