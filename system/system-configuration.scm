(use-modules (gnu))

(use-service-modules desktop networking ssh xorg)
(use-package-modules ssh)

(operating-system
  (locale "fr_FR.utf8")
  (timezone "Europe/Paris")
  (keyboard-layout (keyboard-layout "fr" "bepo" #:options '("ctrl:swapcaps")))
  (host-name "happiness")
  (users (cons* (user-account
                 (name "jeko")
                 (comment "Jérémy Korwin-Zmijowski")
                 (group "users")
                 (home-directory "/home/jeko")
                 (supplementary-groups
                  '("wheel" "netdev" "audio" "video")))
		%base-user-accounts))
  
  (packages %base-packages)
  
  (services
   (append (list (service gnome-desktop-service-type)
		 (set-xorg-configuration
		  (xorg-configuration
		   (keyboard-layout keyboard-layout)))
		 (service openssh-service-type
			  (openssh-configuration
			   (openssh openssh-sans-x)
			   (port-number 2222))))
	   %desktop-services))
  
  (bootloader
   (bootloader-configuration
    (bootloader grub-bootloader)
    (targets (list "/dev/sda"))
    (keyboard-layout #f)))
  
  (swap-devices
   (list
    (swap-space
     (target
      (uuid "44cc42f7-e61f-4e5b-a44e-636a6cf8e55f")))))
  
  (file-systems
   (cons* (file-system
            (mount-point "/")
            (device
             (uuid "292fa66e-6570-4033-a286-34637d6f5af8"
                   'ext4))
            (type "ext4"))
          %base-file-systems)))
